let counter = 1

function addNota() {

    if(document.querySelector("#nota").value === "") {
        alert("“Por favor, insira uma nota.")
        return
    }
    if(document.querySelector("#nota").value < 0 || document.querySelector("#nota").value > 10) {
        alert("A nota digitada é inválida, por favor, insira uma nota válida.")
        return
    }

    // Pegar o valor do input
    const nota = document.querySelector("#nota").value

    // Apagar o valor do input
    document.querySelector("#nota").value = ""

    // Adicionar nota a lista
    document.querySelector("#log").value += `A nota ${counter} foi ${nota}\n`

    counter++
}

function calcNota() {

    // Pegar as notas e transformar em uma lista
    const notas = (document.querySelector("#log").value).replace(/A nota \d+ foi |\n$/g, "").split("\n")

    // Soma todas as notas
    const sumNotas = notas.reduce((acc,current) => Number(acc) + Number(current), 0)
    
    // Calcula a média
    const media = sumNotas/notas.length
    
    // Adiciona média à saída
    document.querySelector(".media").innerHTML = media.toFixed(2)
}
